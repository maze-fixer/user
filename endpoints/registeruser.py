from sqlalchemy import select, insert
from ..db import user_table

def get_user_query(user):
    return select(user_table).where(
        user_table.c.username == user.username
    )

def add_user_query(user):
    return insert(user_table).values(
        username = user.username,
        password = user.password
    )

