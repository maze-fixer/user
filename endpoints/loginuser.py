from sqlalchemy import update
from datetime import datetime
import string
from random import choices
from fastapi import HTTPException

from ..db import user_table

def _session_expired(user):
    if (datetime.now() - user['last_login']).days > 30:
        return True

def validate_user(user, user_result):
    if not user_result:
        raise HTTPException(status_code=404, detail="user not found")
    if not user.password == user_result['password']:
        raise HTTPException(status_code=401, detail="incorrect password")
    # yeah I know this is not optimally secure but usability matters too. It's a conscious choice.

def login_user_query(user_result):
    token = ''.join(choices(string.ascii_uppercase + string.digits, k=16)) # todo: generate a token that is actually secure
    if user_result['session_token'] and not _session_expired(user_result):
        token = user_result['session_token']
    query = update(user_table).where(
        user_table.c.username == user_result.username
    ).values(
        session_token = token,
        last_login = datetime.now()
    )
    return query, token

