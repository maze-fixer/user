from fastapi import HTTPException, APIRouter
from pydantic import BaseModel, constr

router = APIRouter()

from .endpoints.registeruser import get_user_query, add_user_query
from .endpoints.loginuser import validate_user, login_user_query
from .db import execute

class UserIn(BaseModel):
    username:str
    password:str

class UserOut(BaseModel):
    id: int
    username:str

@router.post("/user", response_model=UserOut, tags=["Authentication"])
def register(user:UserIn):
    """Creates a new user with the given parameters"""
    query = get_user_query(user)
    get_user_result = execute(query)
    if get_user_result.one_or_none():
        raise HTTPException(status_code=409, detail=f"a user named '{user.username}' already exists")
    query = add_user_query(user)
    user = execute(query)
    return user

@router.post("/login", response_model = constr(min_length=10,max_length=16), tags=["Authentication"])
def login(user:UserIn):
    """Logs in a user. Returns a bearer token that will be valid for 30 days"""
    query = get_user_query(user)
    user_result = execute(query).one_or_none()
    validate_user(user, user_result)
    query, token = login_user_query(user_result)
    execute(query)
    return token
