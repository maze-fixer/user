import os
from sqlalchemy import create_engine, MetaData
from sqlalchemy import Table, Column, String, DateTime, Integer

# in a more mature setup we would use a different database for each service,
# but on heroku we can only use one
db_url = os.getenv("DATABASE_URL")
if db_url.startswith("postgres://"):
    db_url = db_url.replace("postgres://", "postgresql://", 1)


engine = create_engine(db_url, future=True)

metadata_obj = MetaData()

user_table = Table(
    "user_account", metadata_obj,
    Column('id', Integer, primary_key=True),
    Column('username', String),
    Column('password', String),  #todo: make this hashed
    Column('last_login', DateTime),
    Column('session_token', String)
)

metadata_obj.create_all(engine)

def execute(query):
    """convenience function to execute queries"""
    with engine.begin() as connection:
        return connection.execute(query)
